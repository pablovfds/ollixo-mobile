package empsoft.ufcg.com.ollixo.models;


import java.util.List;

public class Collect {

    private String status;
    private int weight;
    private List<String> typeOfWaste;
    private String costumerId;
    private List<String> days;
    private List<String> turns;

    public Collect(String costumerId, String status, int weight, List<String> typeOfWaste,
                   List<String> days, List<String> turns) {
        this.status = status;
        this.weight = weight;
        this.typeOfWaste = typeOfWaste;
        this.costumerId = costumerId;
        this.days = days;
        this.turns = turns;
    }

    public String getCostumerId() {
        return costumerId;
    }

    public void setCostumerId(String costumerId) {
        this.costumerId = costumerId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public List<String> getTypeOfWaste() {
        return typeOfWaste;
    }

    public void setTypeOfWaste(List<String> typeOfWaste) {
        this.typeOfWaste = typeOfWaste;
    }

    public List<String> getDays() {
        return days;
    }

    public void setDays(List<String> days) {
        this.days = days;
    }

    public List<String> getTurns() {
        return turns;
    }

    public void setTurns(List<String> turns) {
        this.turns = turns;
    }
}
