package empsoft.ufcg.com.ollixo.collectsList;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import empsoft.ufcg.com.ollixo.HomeActivity;
import empsoft.ufcg.com.ollixo.R;
import empsoft.ufcg.com.ollixo.adapters.CollectAdapter;
import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.networking.PartnerService;

public class CollectListActivity extends AppCompatActivity implements CollectListContract.View {

    @BindView(R.id.collects) RecyclerView recyclerViewCollects;
    @BindView(R.id.progressBarCollects) ProgressBar progressBar;

    private CollectListContract.Presenter presenter;
    private CollectAdapter collectAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collect_list);

        ButterKnife.bind(this);

        PartnerService partnerService = new PartnerService();
        presenter = new CollectListPresenterImpl(partnerService, this);
    }

    @Override
    protected void onStart() {
        super.onStart();

        recyclerViewCollects.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerViewCollects.setLayoutManager(layoutManager);

        String token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU4Y2U1Y2Q3MDgwMTMzMDQwMGM" +
                "3MzYxNSIsImlhdCI6MTQ5MTE2ODU3MiwiZXhwIjoxNTA2NzIwNTcyfQ.hiz_791H6h8PsB44VNED" +
                "H0hTn_ftF68eJJDPSvVCHHQ";

        String costumerId = "58c5b9db5059620400da6752";

        presenter.loadCollectList(token, costumerId);

    }

    @Override
    public void setCollectList(List<Collect> collects) {
        this.collectAdapter = new CollectAdapter(this, collects);
        this.recyclerViewCollects.setAdapter(collectAdapter);
    }

    @Override
    public void setServerError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_collects, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_home:
                startActivity(new Intent(this, HomeActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
