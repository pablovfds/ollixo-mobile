package empsoft.ufcg.com.ollixo.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import empsoft.ufcg.com.ollixo.R;
import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.models.StatusCollection;

public class CollectAdapter extends RecyclerView.Adapter<CollectAdapter.ViewHolder> {

    private Context context;
    private List<Collect> collectList;

    public CollectAdapter(Context context, List<Collect> collectList) {
        this.collectList = collectList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.collect_row,
                parent, false);
        return new CollectAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CollectAdapter.ViewHolder holder, int position) {

        List<String> typeOfWasteList = collectList.get(position).getTypeOfWaste();
        List<String> daysList = collectList.get(position).getDays();
        List<String> turnsList = collectList.get(position).getTurns();

        String typeOfWaste =  getListString(typeOfWasteList);
        String days = getListString(daysList);
        String turns = getListString(turnsList);
        String status = getStatus(collectList.get(position).getStatus());

        holder.tv_days.setText(days);
        holder.tv_turns.setText(turns);
        holder.tv_typeOfWaste.setText(typeOfWaste);
        holder.tv_status.setText(status);
    }

    @Override
    public int getItemCount() {
        return collectList.size();
    }

    private String getListString(List<String> list){
        String result = "";
        if (list != null){
            for (int i = 0; i < list.size(); i++) {
                result += list.get(i) + (i == list.size() - 1 ? "" : ", ");
            }
        }
        return result;
    }

    private String getStatus(String status){
        if (status.equals(StatusCollection.ACCEPTED.getValue())) {
            return "Aceita";
        } else if (status.equals(StatusCollection.CANCELED.getValue())) {
            return "Cancelada";
        } else if (status.equals(StatusCollection.CLOSED.getValue())) {
            return "Finalizada";
        } else if (status.equals(StatusCollection.OPENED.getValue())) {
            return "Aberta";
        }
        return "";
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView tv_days;
        TextView tv_turns;
        TextView tv_typeOfWaste;
        TextView tv_status;

        public ViewHolder(View itemView) {
            super(itemView);

            tv_days = (TextView) itemView.findViewById(R.id.tv_days);
            tv_turns = (TextView) itemView.findViewById(R.id.tv_turns);
            tv_typeOfWaste = (TextView) itemView.findViewById(R.id.tv_typeOfWaste);
            tv_status = (TextView) itemView.findViewById(R.id.tv_status);

        }
    }
}
