package empsoft.ufcg.com.ollixo.requestCollect;


import java.util.List;

public interface RequestCollectionContract {
    interface Model {
        interface OnRequestCollectionListener {
            void onWeightError();
            void onTypeOfWasteError();
            void onTrashScheduleError();
            void onCollectionTurnsError();
            void onSuccess();
            void onFailure(String error);
        }

        void sendRequestCollection(String weight, List<String> days, List<String> turnos
                , List<String> typeOfWaste,String costumerId, String token,
                                   OnRequestCollectionListener listener);
    }

    interface View {
        void showProgress();
        void hideProgress();
        void setServerError(String error);
        void setWeightError();
        void setTypeOfWasteError();
        void setTrashScheduleError();
        void setCollectionTurnsError();
        void navigateToMain();
    }

    interface Presenter {
        void validateForm(String weight,List<String> days, List<String> turnos
                , List<String> typeOfWaste,String costumerId, String token);
        void setView(View view);
        void onDestroy();
    }
}
