package empsoft.ufcg.com.ollixo.networking;

import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.models.serverResponse.CollectionListResponse;
import empsoft.ufcg.com.ollixo.models.serverResponse.MessageResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PartnerApi {
    @Headers({"Content-Type: application/json"})
    @POST("/collect")
    Call<MessageResponse> sendCollectionRequest(@Header("Authorization") String authorization,
                                                @Body Collect request);

    @Headers({"Content-Type: application/json"})
    @GET("/collect")
    Call<CollectionListResponse> getCollenctionsByCostumerId(@Header("Authorization") String authorization,
                                                             @Query("costumerId") String costumerId);
}
