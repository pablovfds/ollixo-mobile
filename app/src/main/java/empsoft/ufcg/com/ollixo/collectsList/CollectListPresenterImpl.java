package empsoft.ufcg.com.ollixo.collectsList;

import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.networking.PartnerService;

public class CollectListPresenterImpl implements CollectListContract.Presenter,
        CollectListContract.Model.OnCollectListListener {

    private CollectListContract.View view;
    private CollectListContract.Model model;

    public CollectListPresenterImpl(PartnerService service, CollectListContract.View view) {
        this.model = new CollectListModelImpl(service);
        this.view = view;
    }

    @Override
    public void onSuccess(List<Collect> collects) {
        if (view != null) {
            this.view.hideProgress();
            this.view.setCollectList(collects);
        }
    }

    @Override
    public void onError(String message) {
        if (view != null) {
            this.view.hideProgress();
            this.view.setServerError(message);
        }
    }

    @Override
    public void loadCollectList(String token, String costumerId) {
        if (view != null) {
            this.view.showProgress();
            this.model.loadCollectList(token, costumerId, this);
        }
    }
}
