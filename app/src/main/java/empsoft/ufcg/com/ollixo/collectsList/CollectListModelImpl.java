package empsoft.ufcg.com.ollixo.collectsList;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.models.serverResponse.CollectionListResponse;
import empsoft.ufcg.com.ollixo.models.serverResponse.MessageResponse;
import empsoft.ufcg.com.ollixo.networking.PartnerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CollectListModelImpl implements CollectListContract.Model {

    private PartnerService service;

    public CollectListModelImpl(PartnerService service) {
        this.service = service;
    }

    @Override
    public void loadCollectList(String token, String costumerId, final OnCollectListListener listener) {
        String auth = "Bearer "+ token;

        Call<CollectionListResponse> mService = service.getPartnerApi()
                .getCollenctionsByCostumerId(auth, costumerId);

        mService.enqueue(new Callback<CollectionListResponse>() {
            @Override
            public void onResponse(Call<CollectionListResponse> call, Response<CollectionListResponse> response) {
                if (response.isSuccessful()) {
                    listener.onSuccess(response.body().getCollects());
                } else {
                    Gson gson = new Gson();
                    try {
                        MessageResponse serverResponse = gson.fromJson(response.errorBody().string(),
                                MessageResponse.class);

                        if (serverResponse != null) {
                            listener.onError(serverResponse.getMessage());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(Call<CollectionListResponse> call, Throwable t) {
                call.cancel();
                listener.onError(t.getMessage());
            }
        });
    }
}
