package empsoft.ufcg.com.ollixo.models;

public enum StatusCollection {

    OPENED("OPENED"), ACCEPTED("ACCEPTED"), CANCELED("CANCELED"), CLOSED("CLOSED");

    private final String value;

    StatusCollection(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
