package empsoft.ufcg.com.ollixo.networking;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PartnerService {
    private static final String SERVER_URL = "https://ollixo.herokuapp.com";
    private PartnerApi partnerApi;
    private Retrofit retrofit;

    public PartnerService() {
        if(partnerApi == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(SERVER_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
            partnerApi = retrofit.create(PartnerApi.class);
        }
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public PartnerApi getPartnerApi() {
        return partnerApi;
    }
}
