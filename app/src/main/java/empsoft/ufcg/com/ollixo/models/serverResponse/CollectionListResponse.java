package empsoft.ufcg.com.ollixo.models.serverResponse;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;

public class CollectionListResponse {

    @SerializedName("status")
    private int status;

    @SerializedName("collects")
    private List<Collect> collects;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Collect> getCollects() {
        return collects;
    }

    public void setCollects(List<Collect> collects) {
        this.collects = collects;
    }
}
