package empsoft.ufcg.com.ollixo.collectsList;

import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;

public interface CollectListContract {
    interface Model {
        interface OnCollectListListener {
            void onSuccess(List<Collect> collects);
            void onError(String message);
        }

        void loadCollectList(String token, String costumerId, OnCollectListListener listener);
    }

    interface View {
        void setCollectList(List<Collect> collects);
        void setServerError(String message);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        void loadCollectList(String token, String costumerId);
    }
}
