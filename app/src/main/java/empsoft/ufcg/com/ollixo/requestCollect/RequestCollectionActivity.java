package empsoft.ufcg.com.ollixo.requestCollect;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import empsoft.ufcg.com.ollixo.HomeActivity;
import empsoft.ufcg.com.ollixo.R;
import empsoft.ufcg.com.ollixo.networking.PartnerService;

public class RequestCollectionActivity extends AppCompatActivity
        implements RequestCollectionContract.View {

    @BindView(R.id.segunda) CheckBox segunda;
    @BindView(R.id.terca) CheckBox terca;
    @BindView(R.id.quarta) CheckBox quarta;
    @BindView(R.id.quinta) CheckBox quinta;
    @BindView(R.id.sexta) CheckBox sexta;
    @BindView(R.id.sabado) CheckBox sabado;

    @BindView(R.id.manha) CheckBox manha;
    @BindView(R.id.tarde) CheckBox tarde;
    @BindView(R.id.noite) CheckBox noite;

    @BindView(R.id.plastico) CheckBox plastico;
    @BindView(R.id.papel) CheckBox papel;
    @BindView(R.id.eletronico) CheckBox eletronico;
    @BindView(R.id.remedios) CheckBox remedios;
    @BindView(R.id.vidro) CheckBox vidro;
    @BindView(R.id.metal) CheckBox metal;

    @BindView(R.id.qtd) EditText peso;
    @BindView(R.id.progressBar) ProgressBar progressBar;

    private RequestCollectionContract.Presenter presenter;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_collection);

        ButterKnife.bind(this);

        token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9." +
                "eyJpZCI6IjU4Y2U1Y2Q3MDgwMTMzMDQwMGM3MzYxNSIsImlhdCI6MTQ5MDYyNTAyNSwiZXhwIjox" +
                "NTA2MTc3MDI1fQ.eEFz2Bvqfg6kTGiN3K3QRtD5uAq-QRXaewMMGKbbZ0w";

        PartnerService partnerService = new PartnerService();

        this.presenter = new RequestCollectionPresenterImpl(partnerService);
    }

    @OnClick(R.id.requestCollection)
    public void requestCollection(){

        List<String> listDays = new ArrayList<>();
        List<String> listHour = new ArrayList<>();
        List<String> listTypes = new ArrayList<>();

        remedios = (CheckBox)findViewById(R.id.remedios);
        vidro = (CheckBox)findViewById(R.id.vidro);

        getCheckBoxText(listDays, segunda);
        getCheckBoxText(listDays, terca);
        getCheckBoxText(listDays, quarta);
        getCheckBoxText(listDays, quinta);
        getCheckBoxText(listDays, sexta);
        getCheckBoxText(listDays, sabado);

        getCheckBoxText(listHour, manha);
        getCheckBoxText(listHour, tarde);
        getCheckBoxText(listHour, noite);

        getCheckBoxText(listTypes, plastico);
        getCheckBoxText(listTypes, metal);
        getCheckBoxText(listTypes, papel);
        getCheckBoxText(listTypes, eletronico);
        getCheckBoxText(listTypes, remedios);
        getCheckBoxText(listTypes, vidro);

        String costumerId = "58c5b9db5059620400da6752";

        String weight = peso.getText().toString();

        this.presenter.validateForm(weight, listDays, listHour, listTypes, costumerId, token);
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.presenter.setView(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.presenter.onDestroy();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void setServerError(String error) {
        createDiaglog(error);
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setWeightError() {
        createDiaglog(getString(R.string.dialog_weight));
    }

    @Override
    public void setTypeOfWasteError() {
        createDiaglog(getString(R.string.dialog_tipo));
    }

    @Override
    public void setTrashScheduleError() {
        createDiaglog(getString(R.string.dialog_dia));
    }

    @Override
    public void setCollectionTurnsError() {
        createDiaglog(getString(R.string.dialog_turno));
        Toast.makeText(this, getString(R.string.dialog_turno), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToMain() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.msg_request_success)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(RequestCollectionActivity.this, HomeActivity.class));
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void getCheckBoxText(List<String> list, CheckBox checkBox){
        if (checkBox.isChecked()) {
            list.add(checkBox.getText().toString());
        }
    }

    private void createDiaglog(String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // FIRE ZE MISSILES!
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
