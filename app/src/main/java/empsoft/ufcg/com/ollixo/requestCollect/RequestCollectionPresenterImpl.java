package empsoft.ufcg.com.ollixo.requestCollect;

import java.util.List;

import empsoft.ufcg.com.ollixo.networking.PartnerService;

public class RequestCollectionPresenterImpl implements RequestCollectionContract.Presenter,
        RequestCollectionContract.Model.OnRequestCollectionListener {

    private RequestCollectionContract.Model model;
    private RequestCollectionContract.View view;

    public RequestCollectionPresenterImpl(PartnerService partnerService) {
        this.model = new RequestCollectionModelImpl(partnerService);
    }

    @Override
    public void onWeightError() {
        if (view != null) {
            this.view.hideProgress();
            this.view.setWeightError();
        }
    }

    @Override
    public void onTypeOfWasteError() {
        if (view != null) {
            this.view.hideProgress();
            this.view.setTypeOfWasteError();
        }
    }

    @Override
    public void onTrashScheduleError() {
        if (view != null) {
            this.view.hideProgress();
            this.view.setTrashScheduleError();
        }
    }

    @Override
    public void onCollectionTurnsError() {
        if (view != null) {
            this.view.hideProgress();
            this.view.setCollectionTurnsError();
        }
    }

    @Override
    public void onSuccess() {
        if (view != null) {
            this.view.hideProgress();
            this.view.navigateToMain();
        }
    }

    @Override
    public void onFailure(String error) {
        if (view != null) {
            this.view.hideProgress();
            this.view.setServerError(error);
        }
    }


    @Override
    public void validateForm(String weight, List<String> days, List<String> turnos, List<String> typeOfWaste, String costumerId, String token) {
        if (view != null) {
            this.view.showProgress();
            this.model.sendRequestCollection(weight, days, turnos, typeOfWaste, costumerId, token, this);
        }
    }

    @Override
    public void setView(RequestCollectionContract.View newView) {
        if (newView != null) {
            this.view = newView;
        }
    }

    @Override
    public void onDestroy() {
        this.view = null;
    }
}


