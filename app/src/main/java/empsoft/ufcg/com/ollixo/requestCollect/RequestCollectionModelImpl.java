package empsoft.ufcg.com.ollixo.requestCollect;


import com.google.gson.Gson;

import java.io.IOException;
import java.util.List;

import empsoft.ufcg.com.ollixo.models.Collect;
import empsoft.ufcg.com.ollixo.models.StatusCollection;
import empsoft.ufcg.com.ollixo.models.serverResponse.MessageResponse;
import empsoft.ufcg.com.ollixo.networking.PartnerService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RequestCollectionModelImpl implements RequestCollectionContract.Model {

    private final PartnerService partnerService;

    public RequestCollectionModelImpl(PartnerService partnerService) {
        this.partnerService = partnerService;
    }

    @Override
    public void sendRequestCollection(String weight, List<String> days, List<String> turns,
                                      List<String> typeOfWaste, String costumerId, String token,
                                      final OnRequestCollectionListener listener) {

        if (weight == null || weight.isEmpty() || Integer.parseInt(weight) < 0){
            listener.onWeightError();
        } else if (days == null || days.isEmpty()) {
            listener.onTrashScheduleError();
        } else if (turns == null || turns.isEmpty()) {
            listener.onCollectionTurnsError();
        } else if (typeOfWaste == null || typeOfWaste.isEmpty()) {
            listener.onTypeOfWasteError();
        } else {
            int qdt = Integer.parseInt(weight);
            String status = StatusCollection.OPENED.getValue();
            Collect collect = new Collect(costumerId, status, qdt, typeOfWaste, days, turns);
            String auth = "Bearer "+ token;

            Call<MessageResponse> mService = partnerService.getPartnerApi()
                    .sendCollectionRequest(auth, collect);

            mService.enqueue(new Callback<MessageResponse>() {
                @Override
                public void onResponse(Call<MessageResponse> call, Response<MessageResponse> response) {
                    if (response.isSuccessful()) {
                        listener.onSuccess();
                    } else {
                        Gson gson = new Gson();
                        try {
                            MessageResponse serverResponse = gson.fromJson(response.errorBody().string(),
                                    MessageResponse.class);

                            if (serverResponse != null) {
                                listener.onFailure(serverResponse.getMessage());
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<MessageResponse> call, Throwable t) {
                    call.cancel();
                    listener.onFailure(t.getMessage());
                }
            });
        }
    }
}
